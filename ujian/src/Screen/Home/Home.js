import AsyncStorage from '@react-native-async-storage/async-storage';
import { useNavigation } from '@react-navigation/core';
import React from 'react'
import { StyleSheet, Text, View, Image } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler';
import { heightPercentageToDP, widthPercentageToDP } from 'react-native-responsive-screen';
import { moderateScale } from 'react-native-size-matters';

export default function Home() {

    const navigation = useNavigation()

    const handleLogout = async() => {
        await AsyncStorage.removeItem('access_token')
        navigation.navigate('Login')
    }
    return (
        <>
        
        {/* <View>
            <TouchableOpacity style={styles.buttonContainer} onPress={handleLogout}>
            <Text style={styles.title}>LOGOUT</Text>
            </TouchableOpacity>
        </View> */}
        <View style={styles.container}>
        <Image
        source={require('../../Assets/Images/flasher.png')}
        style={styles.logo}
         />
        <Text style={styles.loginTextAtas}>EMERGENCY</Text>
        <Text style={styles.loginTextMid}>DON'T WORRY</Text>
        <Text style={styles.loginTextDetail}>Tap the panic button below to emergency logout</Text>
  
        <TouchableOpacity
          onPress={handleLogout}
          style={styles.roundButton}>
          <Text style={styles.panicText}>PANIC BUTTON</Text>
        </TouchableOpacity>
      </View>
      </>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor: '#EC3130',
      },
      panicText: {
        fontSize: 14,
        color: 'white',
        fontWeight: 'bold',
        alignItems: 'center',
        justifyContent: 'center',
      },
      roundButton: {
        marginTop: 20,
        width: moderateScale(142),
        height: moderateScale(142),
        justifyContent: 'center',
        alignItems: 'center',
        padding: widthPercentageToDP(5),
        borderRadius: moderateScale(100),
        backgroundColor: '#F40009',
        elevation: 20,
        shadowOpacity: 10,
        // color: 'white',
        // padding: 10,
      },
      loginTextAtas: {
        fontSize: 20,
        color: '#EC3130',
        textAlign: 'center',
        marginTop: heightPercentageToDP(1),
      },
      loginTextMid: {
        fontSize: 35,
        color: '#F40009',
        textAlign: 'center',
        fontWeight: 'bold',
        marginTop: heightPercentageToDP(10),
        // paddingVertical: heightPercentageToDP(5),
        // marginBottom: moderateScale(15)
        // backgroundColor:'red',
      },
      loginTextDetail: {
        fontSize: 15,
        color: '#EC3130',
        textAlign: 'center',
        marginBottom: heightPercentageToDP(5)
      },
      logo: {
        width: moderateScale(150),
        height: moderateScale(150),
        paddingTop: heightPercentageToDP(20),
        marginTop: heightPercentageToDP(2),
        borderWidth:2,
        borderColor: '#F4000940',
        borderRadius:moderateScale(100),
        // top: moderateScale(56),
        // left: moderateScale(60),
        // backgroundColor: 'white',
      },
    
})

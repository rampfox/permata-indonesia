import React, {useState, useEffect} from 'react';
import {StyleSheet, Text, TextInput, View} from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { useNavigation } from '@react-navigation/core';

//API
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';

//styling 
import {moderateScale} from 'react-native-size-matters';
import {heightPercentageToDP, widthPercentageToDP} from 'react-native-responsive-screen';

const Login = props => {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');

    const navigation = useNavigation()

    const handleLogin = async() => {
        try {
            console.log('login', username, password)
            const result = await axios ({
                method : 'POST',
                url: 'https://tasklogin.herokuapp.com/api/login',
                headers: {'Content-Type': 'application/json'},
                data: {username, password}
            })
            AsyncStorage.setItem('access_token', result.data.access_token)
            console.log(result.data.access_token)
            navigation.navigate('Home')
        } catch (error){
            console.log('Gagal Login', error)
            alert ('password salah')
        }
    }
  return (
    <>
      <View style={styles.container}>
        <Text style={styles.loginText}>Sign In</Text>
        <TextInput 
        value={username} 
        onChangeText={setUsername} 
        style={styles.userNameText} 
        placeholderTextColor="#FFFFFF70"
        placeholder={'Username'} 
        />
        <TextInput 
        secureTextEntry 
        value={password} 
        onChangeText={setPassword} 
        style={styles.userNameText} 
        placeholderTextColor="#FFFFFF70" 
        placeholder={'Password'} 
        />
        <TouchableOpacity style={styles.buttonContainer} onPress={handleLogin}>
             <Text style={styles.title}>LOGIN</Text>
        </TouchableOpacity>
      </View>
    </>
  );
};

export default Login;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 10,
    paddingVertical: heightPercentageToDP(20),
    // backgroundColor: '#EC3130',
  },
  loginText: {
    fontSize: 25,
    fontWeight: 'bold',
    color: '#EC3130',
    textAlign: 'center',
    marginBottom: moderateScale(15),
  },
  userNameText: {
    borderWidth: 1,
    borderRadius: 30,
    borderColor: '#EC3130',
    color: 'black',
    marginBottom: 10,
    marginHorizontal: widthPercentageToDP(10),
    paddingLeft: widthPercentageToDP(4),
  },
  buttonContainer: {
    height: 50,
    borderRadius: 30,
    backgroundColor: '#EC3130',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    width: widthPercentageToDP(80),
    marginTop: heightPercentageToDP(5),
  },
  title: {
    fontSize: 16,
    color: 'white',
  },
});
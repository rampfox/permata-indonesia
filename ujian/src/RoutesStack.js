import React from "react";
import {createStackNavigator} from '@react-navigation/stack';
import Login from "./Screen/Login/Login";
import Home from "./Screen/Home/Home";


const Stack = createStackNavigator();

const RouteStack = (navigation) => (
    <Stack.Navigator  screenOptions={{headerShown: false}} initialRouteName='Login'>
        <Stack.Screen name='Login' component={Login}/>
        <Stack.Screen name='Home' component={Home}/>
    </Stack.Navigator>
)

export default RouteStack
import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import RoutesStack from './src/RoutesStack';

const Stack = createStackNavigator();

const App = () => {
  return (
  <NavigationContainer>
    <RoutesStack />
  </NavigationContainer>
  )
}

export default App

const styles = StyleSheet.create({})
